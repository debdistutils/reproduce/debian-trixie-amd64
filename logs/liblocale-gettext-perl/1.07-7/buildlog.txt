+ date
Mon Jul  8 23:24:28 UTC 2024
+ id
uid=0(root) gid=0(root) groups=0(root)
+ pwd
/build/liblocale-gettext-perl
+ apt-get source --only-source liblocale-gettext-perl=1.07-7
Reading package lists...
NOTICE: 'liblocale-gettext-perl' packaging is maintained in the 'Git' version control system at:
https://salsa.debian.org/perl-team/modules/packages/liblocale-gettext-perl.git
Please use:
git clone https://salsa.debian.org/perl-team/modules/packages/liblocale-gettext-perl.git
to retrieve the latest (possibly unreleased) updates to the package.
Need to get 17.6 kB of source archives.
Get:1 http://deb.debian.org/debian trixie/main liblocale-gettext-perl 1.07-7 (dsc) [2458 B]
Get:2 http://deb.debian.org/debian trixie/main liblocale-gettext-perl 1.07-7 (tar) [8651 B]
Get:3 http://deb.debian.org/debian trixie/main liblocale-gettext-perl 1.07-7 (diff) [6516 B]
dpkg-source: info: extracting liblocale-gettext-perl in liblocale-gettext-perl-1.07
dpkg-source: info: unpacking liblocale-gettext-perl_1.07.orig.tar.gz
dpkg-source: info: unpacking liblocale-gettext-perl_1.07-7.debian.tar.xz
dpkg-source: info: using patch list from debian/patches/series
dpkg-source: info: applying C.UTF-8.patch
Fetched 17.6 kB in 0s (39.8 kB/s)
W: Download is performed unsandboxed as root as file 'liblocale-gettext-perl_1.07-7.dsc' couldn't be accessed by user '_apt'. - pkgAcquire::Run (13: Permission denied)
+ ls -la
total 36
drwxr-xr-x 3 root root 4096 Jul  8 23:24 .
drwxr-xr-x 3 root root 4096 Jul  8 23:24 ..
drwxr-xr-x 6 root root 4096 Jul  8 23:24 liblocale-gettext-perl-1.07
-rw-r--r-- 1 root root 6516 Mar 30 00:05 liblocale-gettext-perl_1.07-7.debian.tar.xz
-rw-r--r-- 1 root root 2458 Mar 30 00:05 liblocale-gettext-perl_1.07-7.dsc
-rw-r--r-- 1 root root 8651 Sep 28  2015 liblocale-gettext-perl_1.07.orig.tar.gz
+ find . -maxdepth 1 -type f
+ sha256sum ./liblocale-gettext-perl_1.07.orig.tar.gz ./liblocale-gettext-perl_1.07-7.debian.tar.xz ./liblocale-gettext-perl_1.07-7.dsc
909d47954697e7c04218f972915b787bd1244d75e3bd01620bc167d5bbc49c15  ./liblocale-gettext-perl_1.07.orig.tar.gz
83ecf7ec5fee525b75d1114d8542c5d01e18b08f3a4cdd7d991cbf2a333db2d0  ./liblocale-gettext-perl_1.07-7.debian.tar.xz
2d79df99c4c459114e162660c2db1c7383bf15d6391f621e859cae68fbfc5b66  ./liblocale-gettext-perl_1.07-7.dsc
+ find . -maxdepth 1 -name liblocale-gettext-perl* -type d
+ cd ./liblocale-gettext-perl-1.07
+ env DEBIAN_FRONTEND=noninteractive apt-get build-dep -y --only-source liblocale-gettext-perl=1.07-7
Reading package lists...
Building dependency tree...
Reading state information...
The following NEW packages will be installed:
  autoconf automake autopoint autotools-dev bsdextrautils debhelper
  dh-autoreconf dh-strip-nondeterminism dwz gettext gettext-base groff-base
  intltool-debian libarchive-zip-perl libc-l10n libdebhelper-perl libelf1t64
  libfile-stripnondeterminism-perl libicu72 libperl-dev libpipeline1 libtool
  libuchardet0 libxml2 locales-all m4 man-db po-debconf
0 upgraded, 28 newly installed, 0 to remove and 0 not upgraded.
Need to get 31.7 MB of archives.
After this operation, 309 MB of additional disk space will be used.
Get:1 http://deb.debian.org/debian trixie/main amd64 gettext-base amd64 0.22.5-1 [199 kB]
Get:2 http://deb.debian.org/debian trixie/main amd64 libuchardet0 amd64 0.0.8-1+b1 [68.8 kB]
Get:3 http://deb.debian.org/debian trixie/main amd64 groff-base amd64 1.23.0-4 [1180 kB]
Get:4 http://deb.debian.org/debian trixie/main amd64 libc-l10n all 2.38-13 [724 kB]
Get:5 http://deb.debian.org/debian trixie/main amd64 bsdextrautils amd64 2.40.1-9 [96.2 kB]
Get:6 http://deb.debian.org/debian trixie/main amd64 libpipeline1 amd64 1.5.7-2 [38.0 kB]
Get:7 http://deb.debian.org/debian trixie/main amd64 man-db amd64 2.12.1-2 [1411 kB]
Get:8 http://deb.debian.org/debian trixie/main amd64 m4 amd64 1.4.19-4 [287 kB]
Get:9 http://deb.debian.org/debian trixie/main amd64 autoconf all 2.71-3 [332 kB]
Get:10 http://deb.debian.org/debian trixie/main amd64 autotools-dev all 20220109.1 [51.6 kB]
Get:11 http://deb.debian.org/debian trixie/main amd64 automake all 1:1.16.5-1.3 [823 kB]
Get:12 http://deb.debian.org/debian trixie/main amd64 autopoint all 0.22.5-1 [723 kB]
Get:13 http://deb.debian.org/debian trixie/main amd64 libdebhelper-perl all 13.16 [88.6 kB]
Get:14 http://deb.debian.org/debian trixie/main amd64 libtool all 2.4.7-7 [517 kB]
Get:15 http://deb.debian.org/debian trixie/main amd64 dh-autoreconf all 20 [17.1 kB]
Get:16 http://deb.debian.org/debian trixie/main amd64 libarchive-zip-perl all 1.68-1 [104 kB]
Get:17 http://deb.debian.org/debian trixie/main amd64 libfile-stripnondeterminism-perl all 1.14.0-1 [19.5 kB]
Get:18 http://deb.debian.org/debian trixie/main amd64 dh-strip-nondeterminism all 1.14.0-1 [8448 B]
Get:19 http://deb.debian.org/debian trixie/main amd64 libelf1t64 amd64 0.191-1+b1 [189 kB]
Get:20 http://deb.debian.org/debian trixie/main amd64 dwz amd64 0.15-1+b1 [110 kB]
Get:21 http://deb.debian.org/debian trixie/main amd64 libicu72 amd64 72.1-5 [9396 kB]
Get:22 http://deb.debian.org/debian trixie/main amd64 libxml2 amd64 2.9.14+dfsg-1.3+b3 [692 kB]
Get:23 http://deb.debian.org/debian trixie/main amd64 gettext amd64 0.22.5-1 [1593 kB]
Get:24 http://deb.debian.org/debian trixie/main amd64 intltool-debian all 0.35.0+20060710.6 [22.9 kB]
Get:25 http://deb.debian.org/debian trixie/main amd64 po-debconf all 1.0.21+nmu1 [248 kB]
Get:26 http://deb.debian.org/debian trixie/main amd64 debhelper all 13.16 [891 kB]
Get:27 http://deb.debian.org/debian trixie/main amd64 libperl-dev amd64 5.38.2-5 [1087 kB]
Get:28 http://deb.debian.org/debian trixie/main amd64 locales-all amd64 2.38-13 [10.7 MB]
debconf: delaying package configuration, since apt-utils is not installed
Fetched 31.7 MB in 1s (38.4 MB/s)
Selecting previously unselected package gettext-base.
(Reading database ... (Reading database ... 5%(Reading database ... 10%(Reading database ... 15%(Reading database ... 20%(Reading database ... 25%(Reading database ... 30%(Reading database ... 35%(Reading database ... 40%(Reading database ... 45%(Reading database ... 50%(Reading database ... 55%(Reading database ... 60%(Reading database ... 65%(Reading database ... 70%(Reading database ... 75%(Reading database ... 80%(Reading database ... 85%(Reading database ... 90%(Reading database ... 95%(Reading database ... 100%(Reading database ... 17126 files and directories currently installed.)
Preparing to unpack .../00-gettext-base_0.22.5-1_amd64.deb ...
Unpacking gettext-base (0.22.5-1) ...
Selecting previously unselected package libuchardet0:amd64.
Preparing to unpack .../01-libuchardet0_0.0.8-1+b1_amd64.deb ...
Unpacking libuchardet0:amd64 (0.0.8-1+b1) ...
Selecting previously unselected package groff-base.
Preparing to unpack .../02-groff-base_1.23.0-4_amd64.deb ...
Unpacking groff-base (1.23.0-4) ...
Selecting previously unselected package libc-l10n.
Preparing to unpack .../03-libc-l10n_2.38-13_all.deb ...
Unpacking libc-l10n (2.38-13) ...
Selecting previously unselected package bsdextrautils.
Preparing to unpack .../04-bsdextrautils_2.40.1-9_amd64.deb ...
Unpacking bsdextrautils (2.40.1-9) ...
Selecting previously unselected package libpipeline1:amd64.
Preparing to unpack .../05-libpipeline1_1.5.7-2_amd64.deb ...
Unpacking libpipeline1:amd64 (1.5.7-2) ...
Selecting previously unselected package man-db.
Preparing to unpack .../06-man-db_2.12.1-2_amd64.deb ...
Unpacking man-db (2.12.1-2) ...
Selecting previously unselected package m4.
Preparing to unpack .../07-m4_1.4.19-4_amd64.deb ...
Unpacking m4 (1.4.19-4) ...
Selecting previously unselected package autoconf.
Preparing to unpack .../08-autoconf_2.71-3_all.deb ...
Unpacking autoconf (2.71-3) ...
Selecting previously unselected package autotools-dev.
Preparing to unpack .../09-autotools-dev_20220109.1_all.deb ...
Unpacking autotools-dev (20220109.1) ...
Selecting previously unselected package automake.
Preparing to unpack .../10-automake_1%3a1.16.5-1.3_all.deb ...
Unpacking automake (1:1.16.5-1.3) ...
Selecting previously unselected package autopoint.
Preparing to unpack .../11-autopoint_0.22.5-1_all.deb ...
Unpacking autopoint (0.22.5-1) ...
Selecting previously unselected package libdebhelper-perl.
Preparing to unpack .../12-libdebhelper-perl_13.16_all.deb ...
Unpacking libdebhelper-perl (13.16) ...
Selecting previously unselected package libtool.
Preparing to unpack .../13-libtool_2.4.7-7_all.deb ...
Unpacking libtool (2.4.7-7) ...
Selecting previously unselected package dh-autoreconf.
Preparing to unpack .../14-dh-autoreconf_20_all.deb ...
Unpacking dh-autoreconf (20) ...
Selecting previously unselected package libarchive-zip-perl.
Preparing to unpack .../15-libarchive-zip-perl_1.68-1_all.deb ...
Unpacking libarchive-zip-perl (1.68-1) ...
Selecting previously unselected package libfile-stripnondeterminism-perl.
Preparing to unpack .../16-libfile-stripnondeterminism-perl_1.14.0-1_all.deb ...
Unpacking libfile-stripnondeterminism-perl (1.14.0-1) ...
Selecting previously unselected package dh-strip-nondeterminism.
Preparing to unpack .../17-dh-strip-nondeterminism_1.14.0-1_all.deb ...
Unpacking dh-strip-nondeterminism (1.14.0-1) ...
Selecting previously unselected package libelf1t64:amd64.
Preparing to unpack .../18-libelf1t64_0.191-1+b1_amd64.deb ...
Unpacking libelf1t64:amd64 (0.191-1+b1) ...
Selecting previously unselected package dwz.
Preparing to unpack .../19-dwz_0.15-1+b1_amd64.deb ...
Unpacking dwz (0.15-1+b1) ...
Selecting previously unselected package libicu72:amd64.
Preparing to unpack .../20-libicu72_72.1-5_amd64.deb ...
Unpacking libicu72:amd64 (72.1-5) ...
Selecting previously unselected package libxml2:amd64.
Preparing to unpack .../21-libxml2_2.9.14+dfsg-1.3+b3_amd64.deb ...
Unpacking libxml2:amd64 (2.9.14+dfsg-1.3+b3) ...
Selecting previously unselected package gettext.
Preparing to unpack .../22-gettext_0.22.5-1_amd64.deb ...
Unpacking gettext (0.22.5-1) ...
Selecting previously unselected package intltool-debian.
Preparing to unpack .../23-intltool-debian_0.35.0+20060710.6_all.deb ...
Unpacking intltool-debian (0.35.0+20060710.6) ...
Selecting previously unselected package po-debconf.
Preparing to unpack .../24-po-debconf_1.0.21+nmu1_all.deb ...
Unpacking po-debconf (1.0.21+nmu1) ...
Selecting previously unselected package debhelper.
Preparing to unpack .../25-debhelper_13.16_all.deb ...
Unpacking debhelper (13.16) ...
Selecting previously unselected package libperl-dev:amd64.
Preparing to unpack .../26-libperl-dev_5.38.2-5_amd64.deb ...
Unpacking libperl-dev:amd64 (5.38.2-5) ...
Selecting previously unselected package locales-all.
Preparing to unpack .../27-locales-all_2.38-13_amd64.deb ...
Unpacking locales-all (2.38-13) ...
Setting up libpipeline1:amd64 (1.5.7-2) ...
Setting up libc-l10n (2.38-13) ...
Setting up libicu72:amd64 (72.1-5) ...
Setting up bsdextrautils (2.40.1-9) ...
Setting up libarchive-zip-perl (1.68-1) ...
Setting up libdebhelper-perl (13.16) ...
Setting up gettext-base (0.22.5-1) ...
Setting up m4 (1.4.19-4) ...
Setting up libperl-dev:amd64 (5.38.2-5) ...
Setting up locales-all (2.38-13) ...
Setting up libelf1t64:amd64 (0.191-1+b1) ...
Setting up autotools-dev (20220109.1) ...
Setting up autopoint (0.22.5-1) ...
Setting up autoconf (2.71-3) ...
Setting up dwz (0.15-1+b1) ...
Setting up libuchardet0:amd64 (0.0.8-1+b1) ...
Setting up libxml2:amd64 (2.9.14+dfsg-1.3+b3) ...
Setting up automake (1:1.16.5-1.3) ...
update-alternatives: using /usr/bin/automake-1.16 to provide /usr/bin/automake (automake) in auto mode
update-alternatives: warning: skip creation of /usr/share/man/man1/automake.1.gz because associated file /usr/share/man/man1/automake-1.16.1.gz (of link group automake) doesn't exist
update-alternatives: warning: skip creation of /usr/share/man/man1/aclocal.1.gz because associated file /usr/share/man/man1/aclocal-1.16.1.gz (of link group automake) doesn't exist
Setting up libfile-stripnondeterminism-perl (1.14.0-1) ...
Setting up gettext (0.22.5-1) ...
Setting up libtool (2.4.7-7) ...
Setting up intltool-debian (0.35.0+20060710.6) ...
Setting up dh-autoreconf (20) ...
Setting up dh-strip-nondeterminism (1.14.0-1) ...
Setting up groff-base (1.23.0-4) ...
Setting up po-debconf (1.0.21+nmu1) ...
Setting up man-db (2.12.1-2) ...
Building database of manual pages ...
Setting up debhelper (13.16) ...
Processing triggers for libc-bin (2.38-13) ...
+ env DEB_BUILD_OPTIONS=noautodbgsym nocheck eatmydata dpkg-buildpackage --no-sign
dpkg-buildpackage: info: source package liblocale-gettext-perl
dpkg-buildpackage: info: source version 1.07-7
dpkg-buildpackage: info: source distribution unstable
dpkg-buildpackage: info: source changed by gregor herrmann <gregoa@debian.org>
 dpkg-source --before-build .
dpkg-buildpackage: info: host architecture amd64
 debian/rules clean
dh clean
   dh_clean
 dpkg-source -b .
dpkg-source: info: using source format '3.0 (quilt)'
dpkg-source: info: building liblocale-gettext-perl using existing ./liblocale-gettext-perl_1.07.orig.tar.gz
dpkg-source: info: using patch list from debian/patches/series
dpkg-source: info: building liblocale-gettext-perl in liblocale-gettext-perl_1.07-7.debian.tar.xz
dpkg-source: info: building liblocale-gettext-perl in liblocale-gettext-perl_1.07-7.dsc
 debian/rules binary
dh binary
   dh_update_autotools_config
   dh_autoreconf
   dh_auto_configure
	/usr/bin/perl Makefile.PL INSTALLDIRS=vendor "OPTIMIZE=-g -O2 -Werror=implicit-function-declaration -ffile-prefix-map=/build/liblocale-gettext-perl/liblocale-gettext-perl-1.07=. -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security -fcf-protection -Wdate-time -D_FORTIFY_SOURCE=2" "LD=x86_64-linux-gnu-gcc -g -O2 -Werror=implicit-function-declaration -ffile-prefix-map=/build/liblocale-gettext-perl/liblocale-gettext-perl-1.07=. -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security -fcf-protection -Wl,-z,relro -Wl,-z,now"
checking for gettext... yes
checking for dgettext... yes
checking for ngettext... yes
checking for bind_textdomain_codeset... yes
Checking if your kit is complete...
Looks good
Generating a Unix-style Makefile
Writing Makefile for Locale::gettext
Writing MYMETA.yml and MYMETA.json
   dh_auto_build
	make -j4
make[1]: Entering directory '/build/liblocale-gettext-perl/liblocale-gettext-perl-1.07'
Running Mkbootstrap for gettext ()
"/usr/bin/perl" "/usr/share/perl/5.38/ExtUtils/xsubpp"  -typemap '/usr/share/perl/5.38/ExtUtils/typemap'  gettext.xs > gettext.xsc
chmod 644 "gettext.bs"
"/usr/bin/perl" -MExtUtils::Command::MM -e 'cp_nonempty' -- gettext.bs blib/arch/auto/Locale/gettext/gettext.bs 644
cp gettext.pm blib/lib/Locale/gettext.pm
Please specify prototyping behavior for gettext.xs (see perlxs manual)
mv gettext.xsc gettext.c
x86_64-linux-gnu-gcc -c   -D_REENTRANT -D_GNU_SOURCE -DDEBIAN -fwrapv -fno-strict-aliasing -pipe -I/usr/local/include -D_LARGEFILE_SOURCE -D_FILE_OFFSET_BITS=64 -g -O2 -Werror=implicit-function-declaration -ffile-prefix-map=/build/liblocale-gettext-perl/liblocale-gettext-perl-1.07=. -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security -fcf-protection -Wdate-time -D_FORTIFY_SOURCE=2   -DVERSION=\"1.07\" -DXS_VERSION=\"1.07\" -fPIC "-I/usr/lib/x86_64-linux-gnu/perl/5.38/CORE"   gettext.c
rm -f blib/arch/auto/Locale/gettext/gettext.so
x86_64-linux-gnu-gcc -g -O2 -Werror=implicit-function-declaration -ffile-prefix-map=/build/liblocale-gettext-perl/liblocale-gettext-perl-1.07=. -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security -fcf-protection -Wl,-z,relro -Wl,-z,now  -shared -L/usr/local/lib -fstack-protector-strong  gettext.o  -o blib/arch/auto/Locale/gettext/gettext.so  \
      \
  
chmod 755 blib/arch/auto/Locale/gettext/gettext.so
Manifying 1 pod document
make[1]: Leaving directory '/build/liblocale-gettext-perl/liblocale-gettext-perl-1.07'
dh: command-omitted: The call to "debian/rules override_dh_auto_test" was omitted due to "DEB_BUILD_OPTIONS=nocheck"
   create-stamp debian/debhelper-build-stamp
   dh_prep
   dh_auto_install --destdir=debian/liblocale-gettext-perl/
	make -j4 install DESTDIR=/build/liblocale-gettext-perl/liblocale-gettext-perl-1.07/debian/liblocale-gettext-perl AM_UPDATE_INFO_DIR=no PREFIX=/usr
make[1]: Entering directory '/build/liblocale-gettext-perl/liblocale-gettext-perl-1.07'
"/usr/bin/perl" -MExtUtils::Command::MM -e 'cp_nonempty' -- gettext.bs blib/arch/auto/Locale/gettext/gettext.bs 644
Manifying 1 pod document
Files found in blib/arch: installing files in blib/lib into architecture dependent library tree
Installing /build/liblocale-gettext-perl/liblocale-gettext-perl-1.07/debian/liblocale-gettext-perl/usr/lib/x86_64-linux-gnu/perl5/5.38/auto/Locale/gettext/gettext.so
Installing /build/liblocale-gettext-perl/liblocale-gettext-perl-1.07/debian/liblocale-gettext-perl/usr/lib/x86_64-linux-gnu/perl5/5.38/Locale/gettext.pm
Installing /build/liblocale-gettext-perl/liblocale-gettext-perl-1.07/debian/liblocale-gettext-perl/usr/share/man/man3/Locale::gettext.3pm
make[1]: Leaving directory '/build/liblocale-gettext-perl/liblocale-gettext-perl-1.07'
   dh_installdocs
   dh_installchangelogs
   dh_installman
   debian/rules override_dh_perl
make[1]: Entering directory '/build/liblocale-gettext-perl/liblocale-gettext-perl-1.07'
dh_perl -d
make[1]: Leaving directory '/build/liblocale-gettext-perl/liblocale-gettext-perl-1.07'
   dh_link
   dh_strip_nondeterminism
   dh_compress
   dh_fixperms
   dh_missing
   dh_dwz -a
   dh_strip -a
   dh_makeshlibs -a
   dh_shlibdeps -a
dpkg-shlibdeps: warning: diversions involved - output may be incorrect
 diversion by libc6 from: /lib64/ld-linux-x86-64.so.2
dpkg-shlibdeps: warning: diversions involved - output may be incorrect
 diversion by libc6 to: /lib64/ld-linux-x86-64.so.2.usr-is-merged
   dh_installdeb
   dh_gencontrol
   dh_md5sums
   dh_builddeb
dpkg-deb: building package 'liblocale-gettext-perl' in '../liblocale-gettext-perl_1.07-7_amd64.deb'.
 dpkg-genbuildinfo -O../liblocale-gettext-perl_1.07-7_amd64.buildinfo
 dpkg-genchanges -O../liblocale-gettext-perl_1.07-7_amd64.changes
dpkg-genchanges: info: not including original source code in upload
 dpkg-source --after-build .
dpkg-buildpackage: info: binary and diff upload (original source NOT included)
+ date
Mon Jul  8 23:24:38 UTC 2024
+ cd ..
+ ls liblocale-gettext-perl_1.07-7_amd64.deb
+ sed -e s,_.*,,
+ apt-cache show liblocale-gettext-perl=1.07-7
+ grep ^Version: 
+ sed -e s,^Version: ,,
+ binver=1.07-7
+ binnmuver=1.07-7
+ echo 1.07-7
+ sed s,^[0-9]\+:,,
+ binnmuver=1.07-7
+ echo 1.07-7
+ sed s,^[0-9]\+:,,
+ srcver=1.07-7
+ test -f liblocale-gettext-perl_1.07-7_amd64.deb
+ apt-cache show liblocale-gettext-perl=1.07-7
+ grep -e ^Filename:  -e ^SHA256: 
+ sed -z s,.*Filename: .*/\(.*\)SHA256: \([0-9a-f]\{64\}\).*,\2  \1,g
+ ls -la
total 68
drwxr-xr-x 3 root root  4096 Jul  8 23:24 .
drwxr-xr-x 3 root root  4096 Jul  8 23:24 ..
-rw-r--r-- 1 root root   106 Jul  8 23:24 SHA256SUMS
drwxr-xr-x 7 root root  4096 Jul  8 23:24 liblocale-gettext-perl-1.07
-rw-r--r-- 1 root root  6516 Jul  8 23:24 liblocale-gettext-perl_1.07-7.debian.tar.xz
-rw-r--r-- 1 root root  1318 Jul  8 23:24 liblocale-gettext-perl_1.07-7.dsc
-rw-r--r-- 1 root root  5082 Jul  8 23:24 liblocale-gettext-perl_1.07-7_amd64.buildinfo
-rw-r--r-- 1 root root  2065 Jul  8 23:24 liblocale-gettext-perl_1.07-7_amd64.changes
-rw-r--r-- 1 root root 15008 Jul  8 23:24 liblocale-gettext-perl_1.07-7_amd64.deb
-rw-r--r-- 1 root root  8651 Sep 28  2015 liblocale-gettext-perl_1.07.orig.tar.gz
+ find . -maxdepth 1 -type f
+ sha256sum ./liblocale-gettext-perl_1.07-7_amd64.changes ./liblocale-gettext-perl_1.07.orig.tar.gz ./liblocale-gettext-perl_1.07-7_amd64.buildinfo ./liblocale-gettext-perl_1.07-7_amd64.deb ./liblocale-gettext-perl_1.07-7.debian.tar.xz ./liblocale-gettext-perl_1.07-7.dsc ./SHA256SUMS
19732b07e56bee3bc3b0a7ef9b7acb42f7b543e8560911de2d8cb07cac5666f9  ./liblocale-gettext-perl_1.07-7_amd64.changes
909d47954697e7c04218f972915b787bd1244d75e3bd01620bc167d5bbc49c15  ./liblocale-gettext-perl_1.07.orig.tar.gz
b6573a252232d2f6e7221755850ba094ede70f82f62835012b771876e586a06b  ./liblocale-gettext-perl_1.07-7_amd64.buildinfo
a42f94f3cdec5064e44beb0851c542dc2aabbbb74b77e71e450c03c28b077505  ./liblocale-gettext-perl_1.07-7_amd64.deb
1e48c5148d25a30ae54ce0e213ffc805c5e40195a0c01b29456ed3c986fd2710  ./liblocale-gettext-perl_1.07-7.debian.tar.xz
909df2606d55f32e6c2e0c6787860061c8a1fa051bbf836bc9f44ac79da92eac  ./liblocale-gettext-perl_1.07-7.dsc
c082116d2a4ba054889a091bf2f4970905ac07e053268fa079c88c108a998eea  ./SHA256SUMS
+ tail -v -n+0 SHA256SUMS
==> SHA256SUMS <==
bcc857f39e769e519fa8d4953247d0226d6e95adcdbe94e148900f934dce6035  liblocale-gettext-perl_1.07-7_amd64.deb
+ sha256sum -c SHA256SUMS
liblocale-gettext-perl_1.07-7_amd64.deb: FAILED
sha256sum: WARNING: 1 computed checksum did NOT match
