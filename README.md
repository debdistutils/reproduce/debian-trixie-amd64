
# Rebuild of Top-popcon Debian GNU/Linux 13.x trixie on amd64

This project rebuilds
[Top-popcon Debian GNU/Linux 13.x trixie](https://www.debian.org/releases/trixie/) on
amd64 in a GitLab pipeline, and publish diffoscope
output for any differences.

## Status

We have rebuilt **8%** of
**Top-popcon Debian GNU/Linux 13.x trixie**!  That is **8%** of the
packages we have built, and we have built **100%** or
**50** of the **50** source packages to rebuild.

Top-popcon Debian GNU/Linux 13.x trixie (on amd64) contains binary packages
that corresponds to **50** source packages.  Some binary
packages exists in more than one version, so there is a total of
**50** source packages to rebuild.  Of these we have identical
rebuilds for **4** out of the **50**
builds so far.

We have build logs for **50** rebuilds, which may exceed the
number of total source packages to rebuild when a particular source
package (or source package version) has been removed from the archive.
Of the packages we built, **4** packages could be rebuilt
identically, and there are **43** packages that we could
not rebuilt identically.  Building **3** package had build
failures.  We do not attempt to build **0** packages.

[[_TOC_]]

### Rebuildable packages

The following **4** packages could be built locally to
produce the exact same package that is shipped in the archive.

| Package | Version | Build log | Build job |
| ------- | ------- | --------- | --------- |
| apt | 2.9.6 | [build Mon Jul  8 23:24:39 UTC 2024](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/blob/main/logs/apt/2.9.6/buildlog.txt) | [job 7291703733](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/jobs/7291703733) |
| base-files | 13.3 | [build Tue Jul  9 00:03:19 UTC 2024](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/blob/main/logs/base-files/13.3/buildlog.txt) | [job 7291837343](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/jobs/7291837343) |
| base-passwd | 3.6.4 | [build Tue Jul  9 00:12:51 UTC 2024](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/blob/main/logs/base-passwd/3.6.4/buildlog.txt) | [job 7291876862](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/jobs/7291876862) |
| netbase | 6.4 | [build Tue Jul  9 01:42:13 UTC 2024](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/blob/main/logs/netbase/6.4/buildlog.txt) | [job 7292221746](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/jobs/7292221746) |

### Build failures

The following **3** packages have build failures, making it
impossible to even compare the binary package in the archive with what
we are able to build locally.

| Package | Version | Build log | Build job |
| ------- | ------- | --------- | --------- |
| coreutils | 9.4-3.1 | [build Mon Jul  8 23:24:16 UTC 2024](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/blob/main/logs/coreutils/9.4-3.1/buildlog.txt) | [job 7291703731](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/jobs/7291703731) |
| gcc-14 | 14-20240330-1 | [build Mon Jul  8 23:39:46 UTC 2024](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/blob/main/logs/gcc-14/14-20240330-1/buildlog.txt) | [job 7291756706](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/jobs/7291756706) |
| tar | 1.35+dfsg-3 | [build Tue Jul  9 00:03:46 UTC 2024](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/blob/main/logs/tar/1.35+dfsg-3/buildlog.txt) | [job 7291837339](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/jobs/7291837339) |

### Unrebuildable packages

The following **43** packages unfortunately differ in
some way compared to the version distributed in the archive.  Please
investigate the build log and help us improve things!

| Package | Version | Build log | Jobs | Diffoscope |
| ------- | ------- | --------- | ---- | ---------- |
| acl | 2.3.2-2 | [log](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/blob/main/logs/acl/2.3.2-2/buildlog.txt) | build [7291876861](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/jobs/7291876861) <br> diff [7291876866](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/jobs/7291876866) | [diff 7291876866](https://debdistutils.gitlab.io/-/reproduce/debian-trixie-amd64/-/jobs/7291876866/artifacts/diffoscope/index.html) |
| adduser | 3.137 | [log](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/blob/main/logs/adduser/3.137/buildlog.txt) | build [7291891811](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/jobs/7291891811) <br> diff [7291891820](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/jobs/7291891820) | [diff 7291891820](https://debdistutils.gitlab.io/-/reproduce/debian-trixie-amd64/-/jobs/7291891820/artifacts/diffoscope/index.html) |
| attr | 1:2.5.2-1 | [log](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/blob/main/logs/attr/1:2.5.2-1/buildlog.txt) | build [7291804599](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/jobs/7291804599) <br> diff [7291804604](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/jobs/7291804604) | [diff 7291804604](https://debdistutils.gitlab.io/-/reproduce/debian-trixie-amd64/-/jobs/7291804604/artifacts/diffoscope/index.html) |
| bash | 5.2.21-2.1 | [log](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/blob/main/logs/bash/5.2.21-2.1/buildlog.txt) | build [7291703734](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/jobs/7291703734) <br> diff [7291703739](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/jobs/7291703739) | [diff 7291703739](https://debdistutils.gitlab.io/-/reproduce/debian-trixie-amd64/-/jobs/7291703739/artifacts/diffoscope/index.html) |
| bzip2 | 1.0.8-5.1 | [log](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/blob/main/logs/bzip2/1.0.8-5.1/buildlog.txt) | build [7291781398](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/jobs/7291781398) <br> diff [7291781403](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/jobs/7291781403) | [diff 7291781403](https://debdistutils.gitlab.io/-/reproduce/debian-trixie-amd64/-/jobs/7291781403/artifacts/diffoscope/index.html) |
| cpio | 2.15+dfsg-1 | [log](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/blob/main/logs/cpio/2.15+dfsg-1/buildlog.txt) | build [7291756708](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/jobs/7291756708) <br> diff [7291756719](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/jobs/7291756719) | [diff 7291756719](https://debdistutils.gitlab.io/-/reproduce/debian-trixie-amd64/-/jobs/7291756719/artifacts/diffoscope/index.html) |
| debconf | 1.5.86 | [log](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/blob/main/logs/debconf/1.5.86/buildlog.txt) | build [7291804598](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/jobs/7291804598) <br> diff [7291804603](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/jobs/7291804603) | [diff 7291804603](https://debdistutils.gitlab.io/-/reproduce/debian-trixie-amd64/-/jobs/7291804603/artifacts/diffoscope/index.html) |
| debian-archive-keyring | 2023.4 | [log](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/blob/main/logs/debian-archive-keyring/2023.4/buildlog.txt) | build [7291732393](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/jobs/7291732393) <br> diff [7291732408](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/jobs/7291732408) | [diff 7291732408](https://debdistutils.gitlab.io/-/reproduce/debian-trixie-amd64/-/jobs/7291732408/artifacts/diffoscope/index.html) |
| dpkg | 1.22.6 | [log](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/blob/main/logs/dpkg/1.22.6/buildlog.txt) | build [7291732386](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/jobs/7291732386) <br> diff [7291732394](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/jobs/7291732394) | [diff 7291732394](https://debdistutils.gitlab.io/-/reproduce/debian-trixie-amd64/-/jobs/7291732394/artifacts/diffoscope/index.html) |
| e2fsprogs | 1.47.1-1 | [log](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/blob/main/logs/e2fsprogs/1.47.1-1/buildlog.txt) | build [7292227010](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/jobs/7292227010) <br> diff [7292227015](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/jobs/7292227015) | [diff 7292227015](https://debdistutils.gitlab.io/-/reproduce/debian-trixie-amd64/-/jobs/7292227015/artifacts/diffoscope/index.html) |
| expat | 2.6.2-1 | [log](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/blob/main/logs/expat/2.6.2-1/buildlog.txt) | build [7291732391](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/jobs/7291732391) <br> diff [7291732405](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/jobs/7291732405) | [diff 7291732405](https://debdistutils.gitlab.io/-/reproduce/debian-trixie-amd64/-/jobs/7291732405/artifacts/diffoscope/index.html) |
| findutils | 4.10.0-2 | [log](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/blob/main/logs/findutils/4.10.0-2/buildlog.txt) | build [7291891812](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/jobs/7291891812) <br> diff [7291891821](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/jobs/7291891821) | [diff 7291891821](https://debdistutils.gitlab.io/-/reproduce/debian-trixie-amd64/-/jobs/7291891821/artifacts/diffoscope/index.html) |
| gettext | 0.22.5-1 | [log](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/blob/main/logs/gettext/0.22.5-1/buildlog.txt) | build [7291756710](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/jobs/7291756710) <br> diff [7291756722](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/jobs/7291756722) | [diff 7291756722](https://debdistutils.gitlab.io/-/reproduce/debian-trixie-amd64/-/jobs/7291756722/artifacts/diffoscope/index.html) |
| glibc | 2.38-13 | [log](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/blob/main/logs/glibc/2.38-13/buildlog.txt) | build [7291900239](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/jobs/7291900239) <br> diff [7291900244](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/jobs/7291900244) | [diff 7291900244](https://debdistutils.gitlab.io/-/reproduce/debian-trixie-amd64/-/jobs/7291900244/artifacts/diffoscope/index.html) |
| gnupg2 | 2.2.43-7 | [log](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/blob/main/logs/gnupg2/2.2.43-7/buildlog.txt) | build [7291781399](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/jobs/7291781399) <br> diff [7291781404](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/jobs/7291781404) | [diff 7291781404](https://debdistutils.gitlab.io/-/reproduce/debian-trixie-amd64/-/jobs/7291781404/artifacts/diffoscope/index.html) |
| grep | 3.11-4 | [log](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/blob/main/logs/grep/3.11-4/buildlog.txt) | build [7292177135](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/jobs/7292177135) <br> diff [7292177150](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/jobs/7292177150) | [diff 7292177150](https://debdistutils.gitlab.io/-/reproduce/debian-trixie-amd64/-/jobs/7292177150/artifacts/diffoscope/index.html) |
| gzip | 1.12-1.1 | [log](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/blob/main/logs/gzip/1.12-1.1/buildlog.txt) | build [7291804600](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/jobs/7291804600) <br> diff [7291804605](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/jobs/7291804605) | [diff 7291804605](https://debdistutils.gitlab.io/-/reproduce/debian-trixie-amd64/-/jobs/7291804605/artifacts/diffoscope/index.html) |
| hostname | 3.23+nmu2 | [log](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/blob/main/logs/hostname/3.23+nmu2/buildlog.txt) | build [7291900238](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/jobs/7291900238) <br> diff [7291900243](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/jobs/7291900243) | [diff 7291900243](https://debdistutils.gitlab.io/-/reproduce/debian-trixie-amd64/-/jobs/7291900243/artifacts/diffoscope/index.html) |
| iputils | 3:20240117-1 | [log](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/blob/main/logs/iputils/3:20240117-1/buildlog.txt) | build [7291781402](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/jobs/7291781402) <br> diff [7291781408](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/jobs/7291781408) | [diff 7291781408](https://debdistutils.gitlab.io/-/reproduce/debian-trixie-amd64/-/jobs/7291781408/artifacts/diffoscope/index.html) |
| keyutils | 1.6.3-3 | [log](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/blob/main/logs/keyutils/1.6.3-3/buildlog.txt) | build [7291804601](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/jobs/7291804601) <br> diff [7291804606](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/jobs/7291804606) | [diff 7291804606](https://debdistutils.gitlab.io/-/reproduce/debian-trixie-amd64/-/jobs/7291804606/artifacts/diffoscope/index.html) |
| libcap2 | 1:2.66-5 | [log](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/blob/main/logs/libcap2/1:2.66-5/buildlog.txt) | build [7301202907](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/jobs/7301202907) <br> diff [7301202916](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/jobs/7301202916) | [diff 7301202916](https://debdistutils.gitlab.io/-/reproduce/debian-trixie-amd64/-/jobs/7301202916/artifacts/diffoscope/index.html) |
| libedit | 3.1-20240517-1 | [log](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/blob/main/logs/libedit/3.1-20240517-1/buildlog.txt) | build [7291781401](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/jobs/7291781401) <br> diff [7291781406](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/jobs/7291781406) | [diff 7291781406](https://debdistutils.gitlab.io/-/reproduce/debian-trixie-amd64/-/jobs/7291781406/artifacts/diffoscope/index.html) |
| libgpg-error | 1.49-2 | [log](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/blob/main/logs/libgpg-error/1.49-2/buildlog.txt) | build [7291703732](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/jobs/7291703732) <br> diff [7291703737](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/jobs/7291703737) | [diff 7291703737](https://debdistutils.gitlab.io/-/reproduce/debian-trixie-amd64/-/jobs/7291703737/artifacts/diffoscope/index.html) |
| liblocale-gettext-perl | 1.07-7 | [log](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/blob/main/logs/liblocale-gettext-perl/1.07-7/buildlog.txt) | build [7291703735](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/jobs/7291703735) <br> diff [7291703740](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/jobs/7291703740) | [diff 7291703740](https://debdistutils.gitlab.io/-/reproduce/debian-trixie-amd64/-/jobs/7291703740/artifacts/diffoscope/index.html) |
| libselinux | 3.5-2 | [log](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/blob/main/logs/libselinux/3.5-2/buildlog.txt) | build [7291756707](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/jobs/7291756707) <br> diff [7291756718](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/jobs/7291756718) | [diff 7291756718](https://debdistutils.gitlab.io/-/reproduce/debian-trixie-amd64/-/jobs/7291756718/artifacts/diffoscope/index.html) |
| logrotate | 3.21.0-2 | [log](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/blob/main/logs/logrotate/3.21.0-2/buildlog.txt) | build [7301202909](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/jobs/7301202909) <br> diff [7301202918](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/jobs/7301202918) | [diff 7301202918](https://debdistutils.gitlab.io/-/reproduce/debian-trixie-amd64/-/jobs/7301202918/artifacts/diffoscope/index.html) |
| lvm2 | 2.03.22-1 | [log](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/blob/main/logs/lvm2/2.03.22-1/buildlog.txt) | build [7301202911](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/jobs/7301202911) <br> diff [7301202923](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/jobs/7301202923) | [diff 7301202923](https://debdistutils.gitlab.io/-/reproduce/debian-trixie-amd64/-/jobs/7301202923/artifacts/diffoscope/index.html) |
| ncurses | 6.5-2 | [log](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/blob/main/logs/ncurses/6.5-2/buildlog.txt) | build [7291837340](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/jobs/7291837340) <br> diff [7291837356](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/jobs/7291837356) | [diff 7291837356](https://debdistutils.gitlab.io/-/reproduce/debian-trixie-amd64/-/jobs/7291837356/artifacts/diffoscope/index.html) |
| newt | 0.52.24-2 | [log](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/blob/main/logs/newt/0.52.24-2/buildlog.txt) | build [7291804602](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/jobs/7291804602) <br> diff [7291804607](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/jobs/7291804607) | [diff 7291804607](https://debdistutils.gitlab.io/-/reproduce/debian-trixie-amd64/-/jobs/7291804607/artifacts/diffoscope/index.html) |
| pam | 1.5.3-7 | [log](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/blob/main/logs/pam/1.5.3-7/buildlog.txt) | build [7291756705](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/jobs/7291756705) <br> diff [7291756712](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/jobs/7291756712) | [diff 7291756712](https://debdistutils.gitlab.io/-/reproduce/debian-trixie-amd64/-/jobs/7291756712/artifacts/diffoscope/index.html) |
| perl | 5.38.2-5 | [log](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/blob/main/logs/perl/5.38.2-5/buildlog.txt) | build [7291900237](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/jobs/7291900237) <br> diff [7291900242](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/jobs/7291900242) | [diff 7291900242](https://debdistutils.gitlab.io/-/reproduce/debian-trixie-amd64/-/jobs/7291900242/artifacts/diffoscope/index.html) |
| popt | 1.19+dfsg-1 | [log](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/blob/main/logs/popt/1.19+dfsg-1/buildlog.txt) | build [7291781400](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/jobs/7291781400) <br> diff [7291781405](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/jobs/7291781405) | [diff 7291781405](https://debdistutils.gitlab.io/-/reproduce/debian-trixie-amd64/-/jobs/7291781405/artifacts/diffoscope/index.html) |
| popularity-contest | 1.77 | [log](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/blob/main/logs/popularity-contest/1.77/buildlog.txt) | build [7292238711](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/jobs/7292238711) <br> diff [7292238716](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/jobs/7292238716) | [diff 7292238716](https://debdistutils.gitlab.io/-/reproduce/debian-trixie-amd64/-/jobs/7292238716/artifacts/diffoscope/index.html) |
| procps | 2:4.0.4-4 | [log](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/blob/main/logs/procps/2:4.0.4-4/buildlog.txt) | build [7292204108](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/jobs/7292204108) <br> diff [7292204114](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/jobs/7292204114) | [diff 7292204114](https://debdistutils.gitlab.io/-/reproduce/debian-trixie-amd64/-/jobs/7292204114/artifacts/diffoscope/index.html) |
| readline | 8.2-4 | [log](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/blob/main/logs/readline/8.2-4/buildlog.txt) | build [7291876863](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/jobs/7291876863) <br> diff [7291876868](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/jobs/7291876868) | [diff 7291876868](https://debdistutils.gitlab.io/-/reproduce/debian-trixie-amd64/-/jobs/7291876868/artifacts/diffoscope/index.html) |
| sed | 4.9-2 | [log](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/blob/main/logs/sed/4.9-2/buildlog.txt) | build [7292212948](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/jobs/7292212948) <br> diff [7292212956](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/jobs/7292212956) | [diff 7292212956](https://debdistutils.gitlab.io/-/reproduce/debian-trixie-amd64/-/jobs/7292212956/artifacts/diffoscope/index.html) |
| shadow | 1:4.15.2-3 | [log](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/blob/main/logs/shadow/1:4.15.2-3/buildlog.txt) | build [7292177133](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/jobs/7292177133) <br> diff [7292177142](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/jobs/7292177142) | [diff 7292177142](https://debdistutils.gitlab.io/-/reproduce/debian-trixie-amd64/-/jobs/7292177142/artifacts/diffoscope/index.html) |
| slang2 | 2.3.3-5 | [log](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/blob/main/logs/slang2/2.3.3-5/buildlog.txt) | build [7301202910](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/jobs/7301202910) <br> diff [7301202921](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/jobs/7301202921) | [diff 7301202921](https://debdistutils.gitlab.io/-/reproduce/debian-trixie-amd64/-/jobs/7301202921/artifacts/diffoscope/index.html) |
| sysvinit | 3.09-2 | [log](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/blob/main/logs/sysvinit/3.09-2/buildlog.txt) | build [7291732389](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/jobs/7291732389) <br> diff [7291732399](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/jobs/7291732399) | [diff 7291732399](https://debdistutils.gitlab.io/-/reproduce/debian-trixie-amd64/-/jobs/7291732399/artifacts/diffoscope/index.html) |
| tzdata | 2024a-4 | [log](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/blob/main/logs/tzdata/2024a-4/buildlog.txt) | build [7291837346](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/jobs/7291837346) <br> diff [7291837360](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/jobs/7291837360) | [diff 7291837360](https://debdistutils.gitlab.io/-/reproduce/debian-trixie-amd64/-/jobs/7291837360/artifacts/diffoscope/index.html) |
| ucf | 3.0043+nmu1 | [log](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/blob/main/logs/ucf/3.0043+nmu1/buildlog.txt) | build [7291891815](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/jobs/7291891815) <br> diff [7291891823](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/jobs/7291891823) | [diff 7291891823](https://debdistutils.gitlab.io/-/reproduce/debian-trixie-amd64/-/jobs/7291891823/artifacts/diffoscope/index.html) |
| util-linux | 2.40.1-9 | [log](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/blob/main/logs/util-linux/2.40.1-9/buildlog.txt) | build [7291732388](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/jobs/7291732388) <br> diff [7291732395](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/jobs/7291732395) | [diff 7291732395](https://debdistutils.gitlab.io/-/reproduce/debian-trixie-amd64/-/jobs/7291732395/artifacts/diffoscope/index.html) |
| zlib | 1:1.3.dfsg+really1.3.1-1 | [log](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/blob/main/logs/zlib/1:1.3.dfsg+really1.3.1-1/buildlog.txt) | build [7292204107](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/jobs/7292204107) <br> diff [7292204113](https://gitlab.com/debdistutils/reproduce/debian-trixie-amd64/-/jobs/7292204113) | [diff 7292204113](https://debdistutils.gitlab.io/-/reproduce/debian-trixie-amd64/-/jobs/7292204113/artifacts/diffoscope/index.html) |

### Timestamps

The timestamps of the archives used as input to find out which
packages to reproduce are as follows.  To be precise, these are the
`Date` field in the respectively `Release` file used to construct
the list of packages to evaluate.

When speaking about "current" status of this effort it makes sense to
use the latest timestamp from the set below, which is
**Mon Jul  8 20:22:01 UTC 2024**.

| Archive/Suite | Timestamp |
| ------------- | --------- |
| debian/trixie | Mon, 08 Jul 2024 20:22:01 UTC |
| debian/trixie-updates | Mon, 08 Jul 2024 20:22:01 UTC |
| debian-security/trixie-security | Sun, 07 Jul 2024 12:12:17 UTC |

## License

The content of this repository is automatically generated by
[debdistrebuild](https://gitlab.com/debdistutils/debdistrebuild),
which is published under the
[AGPLv3+](https://www.gnu.org/licenses/agpl-3.0.en.html) and to the
extent the outputs are copyrightable they are released under the same
license.

## Contact

The maintainer of this project is [Simon
Josefsson](https://blog.josefsson.org/).

